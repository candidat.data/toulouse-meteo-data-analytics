resource "google_service_account" "service_account" {
  project      = var.project
  account_id   = "data-collector-sa"
  display_name = "Service Account for the Data collection process"

  depends_on = [google_project_service.apis]
}

resource "google_project_iam_member" "project" {
  project = var.project
  role    = "roles/bigquery.jobUser"
  member  = "serviceAccount:${google_service_account.service_account.email}"

  depends_on = [google_project_service.apis]
}

resource "google_bigquery_dataset_iam_member" "data_editor" {
  project    = var.project
  dataset_id = google_bigquery_dataset.toulouse_meteo_data.dataset_id
  role       = "roles/bigquery.dataEditor"
  member     = "serviceAccount:${google_service_account.service_account.email}"

  depends_on = [google_project_service.apis]
}

resource "google_storage_bucket_iam_member" "backfill_admin" {
  bucket = google_storage_bucket.backfill_bucket.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.service_account.email}"

  depends_on = [google_project_service.apis]
}

resource "google_cloud_run_service_iam_member" "run_invoker" {
  location = google_cloud_run_service.default.location
  project  = google_cloud_run_service.default.project
  service  = google_cloud_run_service.default.name
  role     = "roles/run.invoker"
  member   = "serviceAccount:${google_service_account.service_account.email}"

  depends_on = [google_project_service.apis]
}
