from google.cloud import bigquery

PROJECT_ID = "toulouse-meteo-data-analytics"
DST_TABLE = {
    "project": "toulouse-meteo-data-analytics",
    "dataset_id": "ds_toulouse_meteo_data",
    "table_id": "tb_stations",
}


def run():
    bq = bigquery.Client(project=PROJECT_ID)
    with open("stations.jsonl", "rb") as f:
        bq.load_table_from_file(
            file_obj=f,
            destination=f"{DST_TABLE['project']}.{DST_TABLE['dataset_id']}.{DST_TABLE['table_id']}",
            job_config=bigquery.job.LoadJobConfig(
                source_format="NEWLINE_DELIMITED_JSON",
                autodetect=True,
                write_disposition="WRITE_TRUNCATE",
                create_disposition="CREATE_IF_NEEDED",

            )
        ).result()


if __name__ == '__main__':
    run()
